//
// Emissary
// Copyright (C) 2020 Moe Alam, moeiscool
//
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
var io = new (require('socket.io'))()
//process handlers
var s = require('./libs/process.js')(process,__dirname)
//configuration loader
var config = require('./libs/config.js')(s)
//extenders functions
require('./libs/extenders.js')(s,config)
//basic functions
require('./libs/basic.js')(s,config)
//language loader
var lang = require('./libs/language.js')(s,config)
//database connection : mysql, sqlite3..
require('./libs/sql.js')(s,config)
//authenticator functions : API, dashboard login..
require('./libs/auth.js')(s,config,lang)
//express web server with ejs
var app = require('./libs/webServer.js')(s,config,lang,io)
//web server routes : page handling..
require('./libs/webServerPaths.js')(s,config,lang,app,io)
//websocket connection handlers : login and streams..
require('./libs/socketio.js')(s,config,lang,io)
//user and group functions
require('./libs/user.js')(s,config,lang,app)
//branding functions and config defaults
require('./libs/branding.js')(s,config,lang,app,io)
//on-start actions, daemon(s) starter
require('./libs/startup.js')(s,config,lang)
