var $ = function(selector){
  return document.querySelectorAll(selector)
}
var emissary = Object.assign(emissary,{
    chatWidgetButton: $('#emissary_chatWidgetButton')[0],
    window: $('#emissary_chatWidget')[0],
    getOS: function() {
         var userAgent = navigator.userAgent.toLowerCase();
         var os = "Unknown OS Platform";
         match = ["windows nt 10","windows nt 6.3","windows nt 6.2","windows nt 6.1","windows nt 6.0","windows nt 5.2","windows nt 5.1","windows xp","windows nt 5.0","windows me","win98","win95","win16","macintosh","mac os x","mac_powerpc","android","linux","ubuntu","iphone","ipod","ipad","blackberry","webos"];
         result = ["Windows 10","Windows 8.1","Windows 8","Windows 7","Windows Vista","Windows Server 2003/XP x64","Windows XP","Windows XP","Windows 2000","Windows ME","Windows 98","Windows 95","Windows 3.11","Mac OS X","Mac OS X","Mac OS 9","Android","Linux","Ubuntu","iPhone","iPod","iPad","BlackBerry","Mobile"];
         for (var i = 0; i < match.length; i++) {
              if (userAgent.indexOf(match[i]) !== -1) {
                   os = result[i];
                   break;
              }
         }
         return os;
    },
    getBrowser: function() {
        // Blink engine detection
        var isChrome = (!!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime));
        var isOpera = ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0);
        var isIE = (/*@cc_on!@*/false || !!document.documentMode);
        var isBlink = (isChrome || isOpera) && !!window.CSS;
        var checkList = {
            Opera: isOpera,
            Firefox: (typeof InstallTrigger !== 'undefined'),
            Safari: (/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))),
            "Internet Explorer": isIE,
            Edge: (!isIE && !!window.StyleMedia),
            Chrome: isChrome,
            "Edge Chromium": (isChrome && (navigator.userAgent.indexOf("Edg") != -1)),
        }
        var keys = Object.keys(checkList);
        for (var i = 0; i < keys.length; i++) {
            if(checkList[keys[i]])return {
                name: keys[i],
                isBlink: isBlink
            }
        }
    },
    generatedId: function(x){
        if(!x){x=10};var t = "";var p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < x; i++ )
            t += p.charAt(Math.floor(Math.random() * p.length));
        return t;
    }
})
emissary.chatWidgetButton.onclick = function(e){
    if(this.style.right == '-20px'){
        this.style.right = '10px'
        emissary.window.style.right = "-100%"
    }else{
        this.style.right = '-20px'
        emissary.window.style.right = "10px"
    }
}
