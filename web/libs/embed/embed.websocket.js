console.log('Emissary : Attaching Socket.IO Handler',emissary.connectionUrl)
var connectionKey = localStorage.getItem('emissary_connectionKey')
if(!connectionKey){
    connectionKey = emissary.generatedId(30)
    localStorage.setItem('emissary_connectionKey',connectionKey)
}
emissaryWebsocket = io(emissary.connectionUrl,{
    transports: ['websocket']
})
emissaryWebsocket.on('connect',function(d){
    console.log('Websocket Connected')
    emissaryWebsocket.emit('embed-init',{
        os: emissary.getOS(),
        browser: emissary.getBrowser(),
        userAgent: navigator.userAgent,
        connectionKey: connectionKey,
        groupKey: emissary.groupKey,
        authKey: emissary.authKey,
        page: location.pathname,
        href: location.href,
        lastPage: document.referrer
    })
})
emissaryWebsocket.on('f',function(d){
    console.log(d)
})
emissaryWebsocket.on('embed',function(d){
    console.log(d)
})
window.emissaryWebsocket = emissaryWebsocket
