$(document).ready(function(){
    var loadedVisitors = {}
    var totalVisitors = 0
    var pageVisitsCard = $('#pageVisits')
    var totalVisitorsLabels = $('.total-visitors')
    var pageVisitsTable = pageVisitsCard.find('tbody')
    var setCurrentNumberOfVisitors = function(change){
        totalVisitors += change
        totalVisitorsLabels.text(totalVisitors)
    }
    var drawVisitorRow = function(visitor){
        var pageLabel = `${visitor.lastPage ? `<a class="badge badge-sm badge-primary" title="Previous Page" href="${visitor.lastPage}" target="_blank">${visitor.lastPage}</a> <i class="fa fa-arrow-right"></i> ` : ''}<a class="badge badge-sm badge-primary" title="Current Page" href="${visitor.href}" target="_blank">${visitor.page}</a>`
        pageBadges = ``
        $.each(visitor.pagesOpen,function(n,visit){
            pageBadges += `<div class="mb-2 d-inline-flex mr-2"><a href="${visit.href}" target="_blank" class="badge badge-sm badge-primary">${visit.page}</a></div>`
        })
        if($(`#visitor_${visitor.connectionKey}`).length === 0){
            pageVisitsTable.append(`
                <tr id="visitor_${visitor.connectionKey}">
                  <th scope="row" class="pageLabel">
                    ${pageLabel}
                  </th>
                  <td class="pageBadges">
                    ${pageBadges}
                  </td>
                  <td>
                    ${visitor.ip}
                  </td>
                  <td>
                    ${visitor.browser.name}, ${visitor.os}
                  </td>
                  <td class="timeArrived">
                    ${visitor.timeArrived}
                  </td>
                </tr>
            `)
        }else{
            var existingRowElement = $(`#visitor_${visitor.connectionKey}`)
            existingRowElement.find('.timeArrived').text(visitor.timeArrived)
            existingRowElement.find('.pageLabel').html(pageLabel)
            existingRowElement.find('.pageBadges').html(pageBadges)
        }
    }
    $.webSocket.on('f',function(d){
        switch(d.f){
            case'visitors_active':
                pageVisitsTable.empty()
                $.each(d.visitors,function(n,visitor){
                    drawVisitorRow(visitor)
                    loadedVisitors[visitor.connectionKey + visitor.ip] = visitor
                })
                setCurrentNumberOfVisitors(Object.keys(d.visitors).length)
            break;
            case'visitor_left':
                var visitor = d.visitor
                if(Object.keys(visitor.pagesOpen).length === 0){
                    $(`#visitor_${visitor.connectionKey}`).remove()
                    setCurrentNumberOfVisitors(-1)
                    delete(loadedVisitors[visitor.connectionKey + visitor.ip])
                }else{
                    drawVisitorRow(visitor)
                }
            break;
            case'visitor_arrived':
                var visitor = d.visitor
                drawVisitorRow(visitor)
                if(!loadedVisitors[visitor.connectionKey + visitor.ip]){
                    setCurrentNumberOfVisitors(1)
                }
                loadedVisitors[visitor.connectionKey + visitor.ip] = visitor
            break;
        }
    })
})
