$(document).ready(function(){
    console.log('Rikkudo : Attaching Socket.IO Handler')
    $.webSocket = io({
        transports: ['websocket']
    })
    $.webSocket.on('connect',function(d){
        console.log('Websocket Connected')
        $.webSocket.emit('f',{
            f: 'init',
            ke: $user.ke,
            auth: $user.auth_token,
            uid: $user.uid
        })
    })
    $.webSocket.on('f',function(d){
        switch(d.f){
            case'visitors_active':
            case'visitor_left':
            case'visitor_arrived':
            case'visitor_opened_chat':
            case'visitor_closed_chat':
                console.log(d)
            break;
        }
        console.log(new Date(), d)
    })
})
