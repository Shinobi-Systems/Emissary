var moment = require('moment');
var execSync = require('child_process').execSync;
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var jsonfile = require("jsonfile");
module.exports = function(s,config,lang,io){
    s.clientSocketConnection = {}
    //send data to socket client function
    s.tx = function(z,y,x){
      s.onWebsocketMessageSendExtensions.forEach(function(extender){
          extender(z,y,x)
      })
      if(x){
        return x.broadcast.to(y).emit('f',z)
      };
      io.to(y).emit('f',z);
    }
    s.txToDashcamUsers = function(data,groupKey){
        if(s.group[groupKey] && s.group[groupKey].dashcamUsers){
            Object.keys(s.group[groupKey].dashcamUsers).forEach(function(auth){
                s.tx(data,s.group[groupKey].dashcamUsers[auth].cnid)
            })
        }
    }
    s.txWithSubPermissions = function(z,y,permissionChoices){
        if(typeof permissionChoices==='string'){
            permissionChoices=[permissionChoices]
        }
        if(s.group[z.ke]){
            Object.keys(s.group[z.ke].users).forEach(function(v){
                var user = s.group[z.ke].users[v]
                if(user.details.sub){
                    if(user.details.allmonitors!=='1'){
                        var valid=0
                        var checked=permissionChoices.length
                        permissionChoices.forEach(function(b){
                            if(user.details[b] && user.details[b].indexOf(z.mid)!==-1){
                                ++valid
                            }
                        })
                        if(valid === checked){
                           s.tx(z,user.cnid)
                        }
                    }else{
                        s.tx(z,user.cnid)
                    }
                }else{
                    s.tx(z,user.cnid)
                }
            })
        }
    }

    const streamConnectionAuthentication = (options) => {
        return new Promise( (resolve,reject) => {
            s.knexQuery({
                action: "select",
                columns: "ke,uid,auth,mail,details",
                table: "Users",
                where: [
                    ['ke','=',options.ke],
                    ['auth','=',options.auth],
                    ['uid','=',options.uid],
                ]
            },(err,r) => {
                if(r&&r[0]){
                    resolve(r)
                }else{
                    s.knexQuery({
                        action: "select",
                        columns: "*",
                        table: "API",
                        where: [
                            ['ke','=',options.ke],
                            ['code','=',options.auth],
                            ['uid','=',options.uid],
                        ]
                    },(err,r) => {
                        if(r && r[0]){
                            r = r[0]
                            r.details = JSON.parse(r.details)
                            if(r.details.auth_socket === '1'){
                                s.knexQuery({
                                    action: "select",
                                    columns: "ke,uid,auth,mail,details",
                                    table: "Users",
                                    where: [
                                        ['ke','=',options.ke],
                                        ['uid','=',options.uid],
                                    ]
                                },(err,r) => {
                                    if(r && r[0]){
                                        resolve(r)
                                    }else{
                                        reject('User not found')
                                    }
                                })
                            }else{
                                reject('Permissions for this key do not allow authentication with Websocket')
                            }
                        }else{
                            reject('Not an API key')
                        }
                    })
                }
            })
        })
    }

    const validatedAndBindAuthenticationToSocketConnection = (cn,d,removeListenerOnDisconnect) => {
        if(!d.channel)d.channel = 'MAIN';
        cn.ke = d.ke,
        cn.uid = d.uid,
        cn.auth = d.auth;
        cn.channel = d.channel;
        cn.removeListenerOnDisconnect = removeListenerOnDisconnect;
        cn.socketVideoStream = d.id;
    }

    const createStreamEmitter = (d,cn) => {
        var Emitter,chunkChannel
        if(!d.channel){
            Emitter = s.group[d.ke].activeMonitors[d.id].emitter
            chunkChannel = 'MAIN'
        }else{
            Emitter = s.group[d.ke].activeMonitors[d.id].emitterChannel[parseInt(d.channel)+config.pipeAddition]
            chunkChannel = parseInt(d.channel)+config.pipeAddition
        }
        if(!Emitter){
            cn.disconnect();return;
        }
        return Emitter
    }

    ////socket controller
    io.on('connection', function (cn) {
        //main socket control functions
        cn.on('f',function(d){
            if(!cn.ke&&d.f==='init'){//socket login
                cn.ip=cn.request.connection.remoteAddress;
                tx=function(z){if(!z.ke){z.ke=cn.ke;};cn.emit('f',z);}
                const onFail = (msg) => {
                    tx({ok:false,msg:'Not Authorized',token_used:d.auth,ke:d.ke});cn.disconnect();
                }
                const onSuccess = (r) => {
                    r = r[0];
                    cn.join('GRP_'+d.ke);cn.join('CPU');
                    cn.ke=d.ke,
                    cn.uid=d.uid,
                    cn.auth=d.auth;
                    if(!s.group[d.ke])s.group[d.ke]={};
    //                    if(!s.group[d.ke].vid)s.group[d.ke].vid={};
                    if(!s.group[d.ke].users)s.group[d.ke].users={};
    //                    s.group[d.ke].vid[cn.id]={uid:d.uid};
                    s.group[d.ke].users[d.auth] = {
                        cnid: cn.id,
                        uid: r.uid,
                        mail: r.mail,
                        details: JSON.parse(r.details),
                        logged_in_at: s.timeObject(new Date).format(),
                        login_type: 'Dashboard'
                    }
                    s.clientSocketConnection[cn.id] = cn
                    try{s.group[d.ke].users[d.auth].details=JSON.parse(r.details)}catch(er){}
                    if(s.group[d.ke].users[d.auth].details.get_server_log!=='0'){
                        cn.join('GRPLOG_'+d.ke)
                    }
                    s.group[d.ke].users[d.auth].lang=s.getLanguageFile(s.group[d.ke].users[d.auth].details.lang)
                    s.userLog({ke:d.ke,mid:'$USER'},{type:s.group[d.ke].users[d.auth].lang['Websocket Connected'],msg:{mail:r.mail,id:d.uid,ip:cn.ip}})
                    tx({f:'users_online',users:s.group[d.ke].users})
                    s.tx({f:'user_status_change',ke:d.ke,uid:cn.uid,status:1,user:s.group[d.ke].users[d.auth]},'GRP_'+d.ke)
                    s.knexQuery({
                        action: "select",
                        columns: "*",
                        table: "API",
                        where: [
                            ['ke','=',d.ke],
                            ['uid','=',d.uid],
                        ]
                    },function(err,rrr) {
                        tx({
                            f:'init_success',
                            users:s.group[d.ke].vid,
                            apis:rrr,
                            os:{
                                platform:s.platform,
                                cpuCount:s.coreCount,
                                totalmem:s.totalmem
                            }
                        })
                    })
                    s.onSocketAuthenticationExtensions.forEach(function(extender){
                        extender(r,cn,d,tx)
                    })
                }
                streamConnectionAuthentication(d).then(onSuccess).catch(onFail)
                return;
            }else{
                tx({ok:false,msg:lang.NotAuthorizedText1});
            }
        });
        // super page socket functions
        cn.on('super',function(d){
            if(!cn.init&&d.f=='init'){
                d.ok=s.superAuth({mail:d.mail,pass:d.pass},function(data){
                    cn.mail=d.mail
                    cn.join('$');
                    var tempSessionKey = s.gid(30)
                    cn.superSessionKey = tempSessionKey
                    s.superUsersApi[tempSessionKey] = data
                    s.superUsersApi[tempSessionKey].cnid = cn.id
                    if(!data.$user.tokens)data.$user.tokens = {}
                    data.$user.tokens[tempSessionKey] = {}
                    cn.ip=cn.request.connection.remoteAddress
                    s.userLog({ke:'$',mid:'$USER'},{type:lang['Websocket Connected'],msg:{for:lang['Superuser'],id:cn.mail,ip:cn.ip}})
                    cn.init='super';
                    s.tx({f:'init_success',mail:d.mail,superSessionKey:tempSessionKey},cn.id);
                })
                if(d.ok===false){
                    cn.disconnect();
                }
            }else{
                if(cn.mail&&cn.init=='super'){
                    switch(d.f){
                        case'logs':
                            switch(d.ff){
                                case'delete':
                                    s.knexQuery({
                                        action: "delete",
                                        table: "Logs",
                                        where: {
                                            ke: d.ke,
                                        }
                                    })
                                break;
                            }
                        break;
                        case'system':
                            switch(d.ff){
                                case'update':
                                    s.ffmpegKill()
                                    s.systemLog('Shinobi ordered to update',{
                                        by:cn.mail,
                                        ip:cn.ip
                                    })
                                    var updateProcess = spawn('sh',(s.mainDirectory+'/UPDATE.sh').split(' '),{detached: true})
                                    updateProcess.stderr.on('data',function(data){
                                        s.systemLog('Update Info',data.toString())
                                    })
                                    updateProcess.stdout.on('data',function(data){
                                        s.systemLog('Update Info',data.toString())
                                    })
                                break;
                                case'restart':
                                    //config.webPaths.superApiPrefix+':auth/restart/:script'
                                    d.check=function(x){return d.target.indexOf(x)>-1}
                                    if(d.check('system')){
                                        s.systemLog('Shinobi ordered to restart',{by:cn.mail,ip:cn.ip})
                                        s.ffmpegKill()
                                        exec('pm2 restart '+s.mainDirectory+'/camera.js')
                                    }
                                    if(d.check('cron')){
                                        s.systemLog('Shinobi CRON ordered to restart',{by:cn.mail,ip:cn.ip})
                                        exec('pm2 restart '+s.mainDirectory+'/cron.js')
                                    }
                                    if(d.check('logs')){
                                        s.systemLog('Flush PM2 Logs',{by:cn.mail,ip:cn.ip})
                                        exec('pm2 flush')
                                    }
                                break;
                                case'configure':
                                    s.systemLog('conf.json Modified',{by:cn.mail,ip:cn.ip,old:jsonfile.readFileSync(s.location.config)})
                                    jsonfile.writeFile(s.location.config,d.data,{spaces: 2},function(){
                                        s.tx({f:'save_configuration'},cn.id)
                                    })
                                break;
                            }
                        break;
                    }
                }
            }
        })
         //functions for retrieving cron announcements
         cn.on('cron',function(d){
             if(d.f==='init'){
                 if(config.cron.key){
                     if(config.cron.key===d.cronKey){
                        s.cron={started:moment(),last_run:moment(),id:cn.id};
                     }else{
                         cn.disconnect()
                     }
                 }else{
                     s.cron={started:moment(),last_run:moment(),id:cn.id};
                 }
             }else{
                 if(s.cron&&cn.id===s.cron.id){
                     delete(d.cronKey)
                     switch(d.f){
                         case'filters':
                             s.filterEvents(d.ff,d);
                         break;
                         case's.tx':
                             s.tx(d.data,d.to)
                         break;
                         case's.deleteVideo':
                             s.deleteVideo(d.file)
                         break;
                         case'start':case'end':
                             d.mid='_cron';s.userLog(d,{type:'cron',msg:d.msg})
                         break;
                         default:
                             s.systemLog('CRON : ',d)
                         break;
                     }
                 }else{
                     cn.disconnect()
                 }
             }
         })
        cn.on('disconnect', function () {
            if(cn.socketVideoStream){
                cn.closeSocketVideoStream()
                return
            }
            if(cn.ke){
                if(s.group[cn.ke].users[cn.auth]){
                    if(s.group[cn.ke].users[cn.auth].login_type === 'Dashboard'){
                        s.tx({f:'user_status_change',ke:cn.ke,uid:cn.uid,status:0})
                    }
                    s.userLog({ke:cn.ke,mid:'$USER'},{type:lang['Websocket Disconnected'],msg:{mail:s.group[cn.ke].users[cn.auth].mail,id:cn.uid,ip:cn.ip}})
                    delete(s.group[cn.ke].users[cn.auth]);
                }
            }
            if(cn.cron){
                delete(s.cron);
            }
            if(cn.superSessionKey){
                delete(s.superUsersApi[cn.superSessionKey])
            }
            s.onWebSocketDisconnectionExtensions.forEach(function(extender){
                extender(cn)
            })
            delete(s.clientSocketConnection[cn.id])
        })
        s.onWebSocketConnectionExtensions.forEach(function(extender){
            extender(cn)
        })
    });
}
