var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var os = require('os');
var moment = require('moment');
var request = require('request');
var execSync = require('child_process').execSync;
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var ejs = require('ejs');
var fileupload = require("express-fileupload");
module.exports = function(s,config,lang,app,io){
    if(config.productType === 'Pro'){
        var LdapAuth = require('ldapauth-fork');
    }
    s.renderPage = function(req,res,paths,passables,callback){
        passables.window = {}
        passables.data = req.params
        passables.originalURL = s.getOriginalUrl(req)
        passables.baseUrl = req.protocol+'://'+req.hostname
        passables.config = s.getConfigWithBranding(req.hostname)
        res.render(paths,passables,callback)
    }
    //child node proxy check
    //params = parameters
    //cb = callback
    //res = response, only needed for express (http server)
    //request = request, only needed for express (http server)
    s.closeJsonResponse = function(res,endData){
        res.setHeader('Content-Type', 'application/json')
        res.end(s.prettyPrint(endData))
    }
    //get post data
    s.getPostData = function(req,target,parseJSON){
        if(!target)target = 'data'
        if(!parseJSON)parseJSON = true
        var postData = false
        if(req.query && req.query[target]){
            postData = req.query[target]
        }else{
            postData = req.body[target]
        }
        if(parseJSON === true){
            postData = s.parseJSON(postData)
        }
        return postData
    }
    //get client ip address
    s.getClientIp = function(req){
        return req.headers['cf-connecting-ip']||req.headers["CF-Connecting-IP"]||req.headers["'x-forwarded-for"]||req.connection.remoteAddress;
    }
    ////Pages
    app.enable('trust proxy');
    if(config.webPaths.home !== '/'){
        app.use('/libs',express.static(s.mainDirectory + '/web/libs'))
    }
    app.use(s.checkCorrectPathEnding(config.webPaths.home)+'libs',express.static(s.mainDirectory + '/web/libs'))
    app.use(s.checkCorrectPathEnding(config.webPaths.admin)+'libs',express.static(s.mainDirectory + '/web/libs'))
    app.use(s.checkCorrectPathEnding(config.webPaths.super)+'libs',express.static(s.mainDirectory + '/web/libs'))
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(function (req,res,next){
        res.header("Access-Control-Allow-Origin",'*');
        next()
    })
    app.set('views', s.mainDirectory + '/web');
    app.set('view engine','ejs');
    //add template handler
    if(config.renderPaths.handler!==undefined){require(s.mainDirectory+'/web/'+config.renderPaths.handler+'.js').addHandlers(s,app,io,config)}

    /**
    * API : Logout
    */
    app.get(config.webPaths.apiPrefix+':auth/logout/:ke/:id', function (req,res){
        if(s.group[req.params.ke]&&s.group[req.params.ke].users[req.params.auth]){
            delete(s.api[req.params.auth]);
            delete(s.group[req.params.ke].users[req.params.auth]);
            s.knexQuery({
                action: "update",
                table: "Users",
                update: {
                    auth: '',
                },
                where: [
                    ['auth','=',req.params.auth],
                    ['ke','=',req.params.ke],
                    ['uid','=',req.params.id],
                ]
            })
            res.end(s.prettyPrint({ok:true,msg:'You have been logged out, session key is now inactive.'}))
        }else{
            res.end(s.prettyPrint({ok:false,msg:'This group key does not exist or this user is not logged in.'}))
        }
    });
    /**
    * Page : Login Screen
    */
    app.get(config.webPaths.home, function (req,res){
        s.renderPage(req,res,config.renderPaths.index,{lang:lang,config: s.getConfigWithBranding(req.hostname),screen:'dashboard'})
    });
    /**
    * Page : Administrator Login Screen
    */
    app.get(config.webPaths.admin, function (req,res){
        s.renderPage(req,res,config.renderPaths.index,{lang:lang,config: s.getConfigWithBranding(req.hostname),screen:'admin'})
    });
    /**
    * Page : Superuser Login Screen
    */
    app.get(config.webPaths.super, function (req,res){
        s.renderPage(req,res,config.renderPaths.index,{
            lang: lang,
            config: s.getConfigWithBranding(req.hostname),
            screen: 'super'
        })
    });
    /**
    * API : Get User Info
    */
    app.get(config.webPaths.apiPrefix+':auth/userInfo/:ke',function (req,res){
        var response = {ok:false};
        res.setHeader('Content-Type', 'application/json');
        s.auth(req.params,function(user){
            response.ok = true
            response.user = user
            res.end(s.prettyPrint(response));
        },res,req);
    })
    //login function
    s.deleteFactorAuth=function(r){
        delete(s.factorAuth[r.ke][r.uid])
        if(Object.keys(s.factorAuth[r.ke]).length===0){
            delete(s.factorAuth[r.ke])
        }
    }
    /**
    * API : Login handler. Dashboard, Streamer, Dashcam Administrator, Superuser
    */
    app.post([
        config.webPaths.home,
        config.webPaths.admin,
        config.webPaths.super,
        s.checkCorrectPathEnding(config.webPaths.home)+':screen',
        s.checkCorrectPathEnding(config.webPaths.admin)+':screen',
        s.checkCorrectPathEnding(config.webPaths.super)+':screen',
    ],function (req,res){
        var response = {ok: false};
        req.ip = s.getClientIp(req)
        var screenChooser = function(screen){
            var search = function(screen){
                if(req.url.indexOf(screen) > -1){
                    return true
                }
                return false
            }
            switch(true){
                case search(config.webPaths.admin):
                    return 'admin'
                break;
                case search(config.webPaths.super):
                    return 'super'
                break;
                default:
                    return 'dashboard'
                break;
            }
        }
        // brute check
        if(s.failedLoginAttempts[req.body.mail] && s.failedLoginAttempts[req.body.mail].failCount >= 5){
            if(req.query.json=='true'){
                res.end(s.prettyPrint({ok:false}))
            }else{
                s.renderPage(req,res,config.renderPaths.index,{
                    failedLogin: true,
                    message: lang.failedLoginText1,
                    lang: s.copySystemDefaultLanguage(),
                    config: s.getConfigWithBranding(req.hostname),
                    screen: screenChooser(req.params.screen)
                })
            }
            return false
        }
        //
        renderPage = function(focus,data){
            if(s.failedLoginAttempts[req.body.mail]){
                clearTimeout(s.failedLoginAttempts[req.body.mail].timeout)
                delete(s.failedLoginAttempts[req.body.mail])
            }
            if(req.query.json=='true'){
                delete(data.config)
                data.ok = true;
                res.setHeader('Content-Type', 'application/json');
                res.end(s.prettyPrint(data))
            }else{
                data.screen=req.params.screen
                s.renderPage(req,res,focus,data)
            }
        }
        failedAuthentication = function(board){
            // brute protector
            if(!s.failedLoginAttempts[req.body.mail]){
                s.failedLoginAttempts[req.body.mail] = {
                    failCount : 0,
                    ips : {}
                }
            }
            ++s.failedLoginAttempts[req.body.mail].failCount
            if(!s.failedLoginAttempts[req.body.mail].ips[req.ip]){
                s.failedLoginAttempts[req.body.mail].ips[req.ip] = 0
            }
            ++s.failedLoginAttempts[req.body.mail].ips[req.ip]
            clearTimeout(s.failedLoginAttempts[req.body.mail].timeout)
            s.failedLoginAttempts[req.body.mail].timeout = setTimeout(function(){
                delete(s.failedLoginAttempts[req.body.mail])
            },1000 * 60 * 15)
            // check if JSON
            if(req.query.json === 'true'){
                res.setHeader('Content-Type', 'application/json')
                res.end(s.prettyPrint({ok:false}))
            }else{
                s.renderPage(req,res,config.renderPaths.index,{
                    failedLogin: true,
                    message: lang.failedLoginText2,
                    lang: s.copySystemDefaultLanguage(),
                    config: s.getConfigWithBranding(req.hostname),
                    screen: screenChooser(req.params.screen)
                })
            }
            var logTo = {
                ke: '$',
                mid: '$USER'
            }
            var logData = {
                type: lang['Authentication Failed'],
                msg: {
                    for: board,
                    mail: req.body.mail,
                    ip: req.ip
                }
            }
            if(board === 'super'){
                s.userLog(logTo,logData)
            }else{
                s.knexQuery({
                    action: "select",
                    columns: "ke,uid,details",
                    table: "Users",
                    where: [
                        ['mail','=',req.body.mail],
                    ]
                },(err,r) => {
                    if(r && r[0]){
                        r = r[0]
                        r.details = JSON.parse(r.details)
                        r.lang = s.getLanguageFile(r.details.lang)
                        logData.id = r.uid
                        logData.type = r.lang['Authentication Failed']
                        logTo.ke = r.ke
                    }
                    s.userLog(logTo,logData)
                })
            }
        }
        checkRoute = function(r){
            switch(req.body.function){
                case'cam':
                    s.knexQuery({
                        action: "select",
                        columns: "*",
                        table: "Monitors",
                        where: [
                            ['ke','=',r.ke],
                            ['type','=','dashcam'],
                        ]
                    },(err,rr) => {
                        response.mons = rr
                        renderPage(config.renderPaths.dashcam,{
                            // config: s.getConfigWithBranding(req.hostname),
                            $user: response,
                            lang: r.lang,
                            customAutoLoad: s.customAutoLoadTree
                        })
                    })
                break;
                case'streamer':
                    s.knexQuery({
                        action: "select",
                        columns: "*",
                        table: "Monitors",
                        where: [
                            ['ke','=',r.ke],
                            ['type','=','socket'],
                        ]
                    },(err,rr) => {
                        response.mons=rr;
                        renderPage(config.renderPaths.streamer,{
                            // config: s.getConfigWithBranding(req.hostname),
                            $user: response,
                            lang: r.lang,
                            customAutoLoad: s.customAutoLoadTree
                        })
                    })
                break;
                case'admin':
                    if(!r.details.sub){
                        s.knexQuery({
                            action: "select",
                            columns: "uid,mail,details",
                            table: "Users",
                            where: [
                                ['ke','=',r.ke],
                                ['details','LIKE','%"sub"%'],
                            ]
                        },(err,rr) => {
                            s.knexQuery({
                                action: "select",
                                columns: "*",
                                table: "Monitors",
                                where: [
                                    ['ke','=',r.ke],
                                ]
                            },(err,rrr) => {
                                renderPage(config.renderPaths.admin,{
                                    config: s.getConfigWithBranding(req.hostname),
                                    $user: response,
                                    $subs: rr,
                                    $mons: rrr,
                                    lang: r.lang,
                                    customAutoLoad: s.customAutoLoadTree
                                })
                            })
                        })
                    }else{
                        //not admin user
                        var chosenRender = 'home'
                        if(r.details.landing_page && r.details.landing_page !== '' && config.renderPaths[r.details.landing_page]){
                            chosenRender = r.details.landing_page
                        }
                        renderPage(config.renderPaths[chosenRender],{
                            $user: response,
                            config: s.getConfigWithBranding(req.hostname),
                            lang: r.lang,
                            __dirname: s.mainDirectory,
                            customAutoLoad: s.customAutoLoadTree
                        });
                    }
                break;
                default:
                    var chosenRender = 'home'
                    if(r.details.sub && r.details.landing_page && r.details.landing_page !== '' && config.renderPaths[r.details.landing_page]){
                        chosenRender = r.details.landing_page
                    }
                    renderPage(config.renderPaths[chosenRender],{
                        $user: response,
                        config: s.getConfigWithBranding(req.hostname),
                        lang: r.lang,
                        __dirname: s.mainDirectory,
                        customAutoLoad: s.customAutoLoadTree
                    })
                break;
            }
            s.userLog({ke:r.ke,mid:'$USER'},{type:r.lang['New Authentication Token'],msg:{for:req.body.function,mail:r.mail,id:r.uid,ip:req.ip}})
        //    res.end();
        }
        if(req.body.mail&&req.body.pass){
            req.default=function(){
                s.knexQuery({
                    action: "select",
                    columns: "*",
                    table: "Users",
                    where: [
                        ['mail','=',req.body.mail],
                        ['pass','=',s.createHash(req.body.pass)],
                    ],
                    limit: 1
                },(err,r) => {
                    if(!err && r && r[0]){
                        r=r[0];r.auth=s.md5(s.gid());
                        s.knexQuery({
                            action: "update",
                            table: "Users",
                            update: {
                                auth: r.auth
                            },
                            where: [
                                ['ke','=',r.ke],
                                ['uid','=',r.uid],
                            ]
                        })
                        response = {
                            ok: true,
                            auth_token: r.auth,
                            ke: r.ke,
                            uid: r.uid,
                            mail: r.mail,
                            details: r.details
                        };
                        r.details = JSON.parse(r.details);
                        r.lang = s.getLanguageFile(r.details.lang)
                        const factorAuth = function(cb){
                            req.params.auth = r.auth
                            req.params.ke = r.ke
                            if(r.details.factorAuth === "1"){
                                if(!r.details.acceptedMachines||!(r.details.acceptedMachines instanceof Object)){
                                    r.details.acceptedMachines={}
                                }
                                if(!r.details.acceptedMachines[req.body.machineID]){
                                    req.complete=function(){
                                        s.factorAuth[r.ke][r.uid].function = req.body.function
                                        s.factorAuth[r.ke][r.uid].info = response
                                        clearTimeout(s.factorAuth[r.ke][r.uid].expireAuth)
                                        s.factorAuth[r.ke][r.uid].expireAuth = setTimeout(function(){
                                            s.deleteFactorAuth(r)
                                        },1000*60*15)
                                        renderPage(config.renderPaths.factorAuth,{$user:{
                                            ke:r.ke,
                                            uid:r.uid,
                                            mail:r.mail
                                        },lang:r.lang})
                                    }
                                    if(!s.factorAuth[r.ke]){s.factorAuth[r.ke]={}}
                                    if(!s.factorAuth[r.ke][r.uid]){
                                        s.factorAuth[r.ke][r.uid]={key:s.nid(),user:r}
                                        s.onTwoFactorAuthCodeNotificationExtensions.forEach(function(extender){
                                            extender(r)
                                        })
                                        req.complete()
                                    }else{
                                        req.complete()
                                    }
                                }else{
                                   checkRoute(r)
                                }
                            }else{
                               checkRoute(r)
                            }
                        }
                        if(r.details.sub){
                            s.knexQuery({
                                action: "select",
                                columns: "details",
                                table: "Users",
                                where: [
                                    ['ke','=',r.ke],
                                    ['details','NOT LIKE','%"sub"%'],
                                ],
                            },function(err,rr) {
                                if(rr && rr[0]){
                                    rr=rr[0];
                                    rr.details = JSON.parse(rr.details);
                                    r.details.mon_groups = rr.details.mon_groups;
                                    response.details = JSON.stringify(r.details);
                                    factorAuth()
                                }else{
                                    failedAuthentication(req.body.function)
                                }
                            })
                        }else{
                            factorAuth()
                        }
                    }else{
                        failedAuthentication(req.body.function)
                    }
                })
            }
            if(req.body.function === 'super'){
                if(!fs.existsSync(s.location.super)){
                    res.end(lang.superAdminText)
                    return
                }
                var ok = s.superAuth({
                    mail: req.body.mail,
                    pass: req.body.pass,
                    users: true,
                    md5: true
                },function(data){
                    s.knexQuery({
                        action: "select",
                        columns: "*",
                        table: "Logs",
                        where: [
                            ['ke','=','$'],
                        ],
                        orderBy: ['time','desc'],
                        limit: 30
                    },(err,r) => {
                        if(!r){
                            r=[]
                        }
                        data.Logs = r
                        data.customAutoLoad = s.customAutoLoadTree
                        data.currentVersion = s.currentVersion
                        fs.readFile(s.location.config,'utf8',function(err,file){
                            data.plainConfig = JSON.parse(file)
                            renderPage(config.renderPaths.super,data)
                        })
                    })
                })
                if(ok === false){
                    failedAuthentication(req.body.function)
                }
            }else{
                req.default()
            }
        }else{
            if(req.body.machineID&&req.body.factorAuthKey){
                if(s.factorAuth[req.body.ke]&&s.factorAuth[req.body.ke][req.body.id]&&s.factorAuth[req.body.ke][req.body.id].key===req.body.factorAuthKey){
                    if(s.factorAuth[req.body.ke][req.body.id].key===req.body.factorAuthKey){
                        if(req.body.remember==="1"){
                            req.details=JSON.parse(s.factorAuth[req.body.ke][req.body.id].info.details)
                            req.lang=s.getLanguageFile(req.details.lang)
                            if(!req.details.acceptedMachines||!(req.details.acceptedMachines instanceof Object)){
                                req.details.acceptedMachines={}
                            }
                            if(!req.details.acceptedMachines[req.body.machineID]){
                                req.details.acceptedMachines[req.body.machineID]={}
                                s.knexQuery({
                                    action: "update",
                                    table: "Users",
                                    update: {
                                        details: s.prettyPrint(req.details)
                                    },
                                    where: [
                                        ['ke','=',req.body.ke],
                                        ['uid','=',req.body.id],
                                    ]
                                })
                            }
                        }
                        req.body.function = s.factorAuth[req.body.ke][req.body.id].function
                        response = s.factorAuth[req.body.ke][req.body.id].info
                        response.lang = req.lang || s.getLanguageFile(JSON.parse(s.factorAuth[req.body.ke][req.body.id].info.details).lang)
                        checkRoute(s.factorAuth[req.body.ke][req.body.id].info)
                        clearTimeout(s.factorAuth[req.body.ke][req.body.id].expireAuth)
                        s.deleteFactorAuth({
                            ke: req.body.ke,
                            uid: req.body.id,
                        })
                    }else{
                        var info = s.factorAuth[req.body.ke][req.body.id].info
                        renderPage(config.renderPaths.factorAuth,{$user:{
                            ke: info.ke,
                            id: info.uid,
                            mail: info.mail,
                        },lang:req.lang});
                        res.end();
                    }
                }else{
                    failedAuthentication(lang['2-Factor Authentication'])
                }
            }else{
                failedAuthentication(lang['2-Factor Authentication'])
            }
        }
    })
    /**
    * API : Brute Protection Lock Reset by API
    */
    app.get([config.webPaths.apiPrefix+':auth/resetBruteProtection/:ke'], function (req,res){
        s.auth(req.params,function(user){
            if(s.failedLoginAttempts[user.mail]){
                clearTimeout(s.failedLoginAttempts[user.mail].timeout)
                delete(s.failedLoginAttempts[user.mail])
            }
            res.end(s.prettyPrint({ok:true}))
        })
    })
    /**
    * API : Get Logs
     */
    app.get([
        config.webPaths.apiPrefix+':auth/logs/:ke',
        config.webPaths.apiPrefix+':auth/logs/:ke/:id'
    ], function (req,res){
        res.setHeader('Content-Type', 'application/json');
        s.auth(req.params,function(user){
            const userDetails = user.details
            const monitorId = req.params.id
            const groupKey = req.params.ke
            const hasRestrictions = userDetails.sub && userDetails.allmonitors !== '1';
            s.sqlQueryBetweenTimesWithPermissions({
                table: 'Logs',
                user: user,
                groupKey: req.params.ke,
                monitorId: req.params.id,
                startTime: req.query.start,
                endTime: req.query.end,
                startTimeOperator: req.query.startOperator,
                endTimeOperator: req.query.endOperator,
                limit: req.query.limit || 50,
                endIsStartTo: true,
                noFormat: true,
                noCount: true,
                rowName: 'logs',
                preliminaryValidationFailed: (
                    user.permissions.get_logs === "0" || userDetails.sub && userDetails.view_logs !== '1'
                )
            },(response) => {
                response.forEach(function(v,n){
                    v.info = JSON.parse(v.info)
                })
                res.end(s.prettyPrint(response))
            })
        },res,req)
    })
    // /**
    // * API : Upload Video File
    // * API : Add "streamIn" query string to Push to "Dashcam (Streamer v2)" FFMPEG Process
    //  */
    // app.post(config.webPaths.apiPrefix+':auth/videos/:ke/:id',fileupload(), async (req,res) => {
    //     var response = {ok:false}
    //     res.setHeader('Content-Type', 'application/json');
    //     s.auth(req.params,function(user){
    //         if(user.permissions.watch_videos==="0"||user.details.sub&&user.details.allmonitors!=='1'&&user.details.video_delete.indexOf(req.params.id)===-1){
    //             res.end(user.lang['Not Permitted'])
    //             return
    //         }
    //         var groupKey = req.params.ke
    //         var monitorId = req.params.id
    //         // req.query.overwrite === '1'
    //         if(s.group[groupKey] && s.group[groupKey].activeMonitors && s.group[groupKey].activeMonitors[monitorId]){
    //             var monitor = s.group[groupKey].rawMonitorConfigurations[monitorId]
    //             try {
    //                 if(!req.files) {
    //                     res.send({
    //                         status: false,
    //                         message: 'No file uploaded'
    //                     });
    //                 } else {
    //                     let video = req.files.video;
    //                     var time = new Date(parseInt(video.name.split('.')[0]))
    //                     var filename = s.formattedTime(time) + '.' + monitor.ext
    //                     video.mv(s.getVideoDirectory(monitor) +  filename,function(){
    //                         s.insertCompletedVideo(monitor,{
    //                             file: filename,
    //                             events: s.group[groupKey].activeMonitors[monitorId].detector_motion_count,
    //                             endTime: req.body.endTime.indexOf('-') > -1 ? s.nameToTime(req.body.endTime) : parseInt(req.body.endTime) || null,
    //                         },function(){
    //                             response.ok = true
    //                             response.filename = filename
    //                             res.end(s.prettyPrint({
    //                                 ok: true,
    //                                 message: 'File is uploaded',
    //                                 data: {
    //                                     name: video.name,
    //                                     mimetype: video.mimetype,
    //                                     size: video.size
    //                                 }
    //                             }))
    //                         })
    //                     });
    //                 }
    //             } catch (err) {
    //                 response.err = err
    //                 res.status(500).end(response)
    //             }
    //         }else{
    //             response.error = 'Non Existant Monitor'
    //             res.end(s.prettyPrint(response))
    //         }
    //     },res,req);
    // })
    /**
    * API : Account Edit from Dashboard
     */
    app.all(config.webPaths.apiPrefix+':auth/accounts/:ke/edit',function (req,res){
        s.auth(req.params,function(user){
            var endData = {
                ok : false
            }
            var form = s.getPostData(req)
            if(form){
                endData.ok = true
                s.accountSettingsEdit({
                    ke: req.params.ke,
                    uid: user.uid,
                    form: form,
                    cnid: user.cnid
                })
            }else{
                endData.msg = lang.postDataBroken
            }
            s.closeJsonResponse(res,endData)
        },res,req)
    })
    /**
    * API : Embed Widget
    */
    app.get([config.webPaths.apiPrefix+':auth/embed/:ke'], function (req,res){
        s.auth(req.params,function(user){
            s.renderPage(req,res,config.renderPaths.embed,{
                lang: lang,
                authKey: req.params.auth,
                groupKey: req.params.ke,
                connectionProtocol: req.protocol,
                connectionUrl: req.hostname + ':' + config.port,
                config: s.getConfigWithBranding(req.hostname)
            })
        })
    })
    /**
    * API : Get Language JSON
     */
    app.get(config.webPaths.apiPrefix+':auth/language/:ke',function (req,res){
        s.auth(req.params,function(user){
            var endData = {
                ok: true,
                definitions: s.getLanguageFile(user.details.lang)
            }
            s.closeJsonResponse(res,endData)
        },res,req)
    })
    /**
    * Robots.txt
    */
    app.get('/robots.txt', function (req,res){
        res.on('finish',function(){
            res.end()
        })
        fs.createReadStream(s.mainDirectory + '/web/pages/robots.txt').pipe(res)
    })
}
