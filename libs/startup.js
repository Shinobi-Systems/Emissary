
var fs = require('fs');
var request  = require('request');
var moment = require('moment');
var crypto = require('crypto');
var exec = require('child_process').exec;
var execSync = require('child_process').execSync;
module.exports = function(s,config,lang,io){
    var checkedAdminUsers = {}
    console.log('Node.js version : '+process.version)
    s.processReady = function(){
        delete(checkedAdminUsers)
        s.systemLog(lang.startUpText5)
        s.onProcessReadyExtensions.forEach(function(extender){
            extender(true)
        })
        process.send('ready')
    }
    var loadedAccounts = []
    var foundMonitors = []
    var loadAdminUsers = function(callback){
        //get current disk used for each isolated account (admin user) on startup
        s.knexQuery({
            action: "select",
            columns: "*",
            table: "Users",
            where: [
                ['details','NOT LIKE','%"sub"%']
            ]
        },function(err,users) {
            if(users && users[0]){
                users.forEach(function(user){
                    s.debugLog(`Loaded Group Holder`,user.ke, user.mail)
                    checkedAdminUsers[user.ke] = user
                })
                callback()
            }else{
                callback()
            }
        })
    }
    config.userHasSubscribed = false
    var checkSubscription = function(callback){
        var subscriptionFailed = function(){
            console.error('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            console.error('This Install of Emissary is NOT Activated')
            console.error('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            s.systemLog('This Install of Emissary is NOT Activated')
            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            console.log('https://licenses.shinobi.video/subscribe')
        }
        if(config.subscriptionId && config.subscriptionId !== 'sub_XXXXXXXXXXXX'){
            var url = 'https://licenses.shinobi.video/subscribe/check?subscriptionId=' + config.subscriptionId
            request(url,{
                method: 'GET',
                timeout: 30000
            }, function(err,resp,body){
                var json = s.parseJSON(body)
                if(err)console.log(err,json)
                var hasSubcribed = json && !!json.ok
                config.userHasSubscribed = hasSubcribed
                callback(hasSubcribed)
                if(config.userHasSubscribed){
                    s.systemLog('This Install of Shinobi is Activated')
                    if(!json.expired){
                        s.systemLog(`This License expires on ${json.timeExpires}`)
                    }
                }else{
                    subscriptionFailed()
                }
            })
        }else{
            subscriptionFailed()
            callback(false)
        }
    }
    s.databaseEngine = require('knex')(s.databaseOptions)
    setTimeout(() => {
        //check for subscription
        checkSubscription(function(){
            //load administrators (groups)
            loadAdminUsers(function(){
                s.processReady()
            })
        })
    },1500)
}
