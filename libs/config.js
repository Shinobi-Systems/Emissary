module.exports = function(s){
    s.location = {
        super : s.mainDirectory+'/super.json',
        config : s.mainDirectory+'/conf.json',
        languages : s.mainDirectory+'/languages'
    }
    try{
        var config = require(s.location.config)
    }catch(err){
        var config = {}
    }
    if(config.ip === undefined||config.ip===''||config.ip.indexOf('0.0.0.0')>-1){config.ip='localhost'}else{config.bindip=config.ip};
    if(config.iconURL === undefined){config.iconURL = "https://shinobi.video/libs/assets/icon/apple-touch-icon-152x152.png"}
    return config
}
