module.exports = function(s,config,lang,app){
    const userSessions = {}
    const setPageOpen = (visitorInfo,newVisitorInfo,cnId) => {
        visitorInfo.page = newVisitorInfo.page
        visitorInfo.lastPage = newVisitorInfo.lastPage
        visitorInfo.pagesOpen[cnId] = {
            timeArrived: newVisitorInfo.timeArrived,
            page: newVisitorInfo.page,
            href: newVisitorInfo.href,
            lastPage: newVisitorInfo.lastPage,
        }
    }
    const closePageOpen = (visitorInfo,cnId) => {
        delete(visitorInfo.pagesOpen[cnId])
    }
    const closeVisitorSessionIfNoPagesOpen = (visitorInfo) => {
        if(Object.keys(visitorInfo.pagesOpen).length === 0)delete(userSessions[visitorInfo.groupKey][visitorInfo.connectionKey + visitorInfo.ip])
    }
    s.onWebSocketConnection((cn) => {
        const sendData = (data) => {
            cn.emit('embed',data)
        }
        // connection = cn
        cn.on('embed-init',(d) => {
            const groupKey = d.groupKey
            const authKey = d.authKey
            const connectionKey = d.connectionKey
            const clientBrowser = d.browser
            const clientOS = d.os
            const ipAddress = cn.request.connection.remoteAddress
            const timeArrived = new Date()
            const newVisitorInfo = {
                timeArrived: timeArrived,
                connectionKey: connectionKey,
                browser: clientBrowser,
                os: clientOS,
                ip: ipAddress,
                groupKey: groupKey,
                authKey: authKey,
                pagesOpen: {},
                page: d.page,
                href: d.href,
                lastPage: d.lastPage,
            }
            if(!userSessions[groupKey])userSessions[groupKey] = {}
            if(!userSessions[groupKey][connectionKey + ipAddress]){
                userSessions[groupKey][connectionKey + ipAddress] = newVisitorInfo
            }
            const visitorInfo = userSessions[groupKey][connectionKey + ipAddress]
            setPageOpen(
                visitorInfo,
                newVisitorInfo,
                cn.id
            )
            cn.visitorInfo = visitorInfo
            s.tx({
                f: 'visitor_arrived',
                visitor: visitorInfo,
                timeArrived: timeArrived,
                connectionId: cn.id,
            },'GRP_' + groupKey)
        })
    })
    s.onWebSocketDisconnection((cn) => {
        if(cn.visitorInfo){
            const groupKey = cn.visitorInfo.groupKey
            const connectionKey = cn.visitorInfo.connectionKey
            const ipAddress = cn.visitorInfo.ip
            closePageOpen(userSessions[groupKey][connectionKey + ipAddress],cn.id)
            console.log(cn.visitorInfo)
            s.tx({
                f: 'visitor_left',
                visitor: cn.visitorInfo,
                timeLeft: new Date(),
                connectionId: cn.id,
            },'GRP_' + groupKey)
            closeVisitorSessionIfNoPagesOpen(userSessions[groupKey][connectionKey + ipAddress])
        }
    })
    s.onSocketAuthentication((userRow,connection,initData,sendBack) => {
        sendBack({
            f: 'visitors_active',
            visitors: userSessions[userRow.ke] || {}
        })
    })
    /**
    * API : Get Current Visitors
     */
    app.get([
        config.webPaths.apiPrefix+':auth/visitors/:ke',
        config.webPaths.apiPrefix+':auth/visitors/:ke/:id'
    ], function (req,res){
        res.setHeader('Content-Type', 'application/json');
        s.auth(req.params,function(user){
            s.closeJsonResponse((res,req.params.id ? userSessions[req.params.ke][req.params.id] : userSessions[req.params.ke]) || {})
        },res,req)
    })
    return {
        userSessions: userSessions
    }
}
