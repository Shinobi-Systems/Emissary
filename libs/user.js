var fs = require('fs');
var events = require('events');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var async = require("async");
module.exports = function(s,config,lang,app){
    const {
        userSessions
    } = require('./user/sessions.js')(s,config,lang,app)
    s.group = {}
    s.userLog = function(e,x){
        if(e.id && !e.mid)e.mid = e.id
        if(!x||!e.mid){return}
        if(
            (e.details && e.details.sqllog === '1') ||
            e.mid.indexOf('$') > -1
        ){
            s.knexQuery({
                action: "insert",
                table: "Logs",
                insert: {
                    ke: e.ke,
                    mid: e.mid,
                    info: s.s(x),
                }
            })
        }
        s.tx({f:'log',ke:e.ke,mid:e.mid,log:x,time:s.timeObject()},'GRPLOG_'+e.ke);
    }
    s.loadGroup = function(e){
        s.loadGroupExtensions.forEach(function(extender){
            extender(e)
        })
        if(!s.group[e.ke]){
            s.group[e.ke]={}
        }
        if(!s.group[e.ke].init){
            s.group[e.ke].init={}
        }
        if(!s.group[e.ke].users){s.group[e.ke].users={}}
    }
    s.loadGroupApps = function(e){
        // e = user
        if(!s.group[e.ke].init){
            s.group[e.ke].init={};
        }
        s.knexQuery({
            action: "select",
            columns: "*",
            table: "Users",
            where: [
                ['ke','=',e.ke],
                ['details','NOT LIKE',`%"sub"%`],
            ],
            limit: 1
        },(err,r) => {
            if(r && r[0]){
                r = r[0];
                const details = JSON.parse(r.details);
                s.loadGroupAppExtensions.forEach(function(extender){
                    extender(r,details)
                })
                Object.keys(details).forEach(function(v){
                    s.group[e.ke].init[v] = details[v]
                })
            }
        })
    }
    s.accountSettingsEdit = function(d,dontRunExtensions){
        s.knexQuery({
            action: "select",
            columns: "details",
            table: "Users",
            where: [
                ['ke','=',d.ke],
                ['uid','=',d.uid],
            ]
        },(err,r) => {
            if(r && r[0]){
                r = r[0];
                const details = JSON.parse(r.details);
                if(!details.sub || details.user_change !== "0"){
                    if(d.cnid){
                        if(details.get_server_log === '1'){
                            s.clientSocketConnection[d.cnid].join('GRPLOG_'+d.ke)
                        }else{
                            s.clientSocketConnection[d.cnid].leave('GRPLOG_'+d.ke)
                        }
                    }
                    ///unchangeable from client side, so reset them in case they did.
                    var form = d.form
                    var formDetails = JSON.parse(form.details)
                    if(!dontRunExtensions){
                        s.beforeAccountSaveExtensions.forEach(function(extender){
                            extender({
                                form: form,
                                formDetails: formDetails,
                                d: details
                            })
                        })
                    }
                    //admin permissions
                    formDetails.permissions = details.permissions
                    formDetails.edit_size = details.edit_size
                    formDetails.edit_days = details.edit_days
                    formDetails.use_admin = details.use_admin
                    //check
                    if(details.edit_days == "0"){
                        formDetails.days = details.days;
                    }
                    if(details.edit_size == "0"){
                        formDetails.size = details.size;
                        formDetails.addStorage = details.addStorage;
                    }
                    if(details.sub){
                        formDetails.sub = details.sub;
                        if(details.days){formDetails.days = details.days;}
                    }
                    ///
                    formDetails = JSON.stringify(s.mergeDeep(details,formDetails))
                    ///
                    const updateQuery = {}
                    if(form.pass && form.pass !== ''){
                        form.pass = s.createHash(form.pass)
                    }else{
                        delete(form.pass)
                    }
                    delete(form.password_again)
                    Object.keys(form).forEach(function(key){
                        const value = form[key]
                        updateQuery[key] = value
                    })
                    s.knexQuery({
                        action: "update",
                        table: "Users",
                        update: updateQuery,
                        where: [
                            ['ke','=',d.ke],
                            ['uid','=',d.uid],
                        ]
                    },() => {
                        if(!details.sub){
                            var user = Object.assign(form,{ke : d.ke})
                            var userDetails = JSON.parse(formDetails)
                            if(!dontRunExtensions){
                                s.onAccountSaveExtensions.forEach(function(extender){
                                    extender(s.group[d.ke],userDetails,user)
                                })
                                s.unloadGroupAppExtensions.forEach(function(extender){
                                    extender(user)
                                })
                                s.loadGroupApps(d)
                            }
                        }
                        if(d.cnid)s.tx({f:'user_settings_change',uid:d.uid,ke:d.ke,form:form},d.cnid)
                    })
                }
            }
        })
    }
}
